terraform {
  backend "s3" {
    bucket = "terraform-state-ghazalehca1234"
    key    = "terraform.tfstate"
    region = "eu-west-1"
  }
}